#include "NodoLista.h"

// Constructor por defecto
template<typename T>
 
NodoLista<T>::NodoLista()
{
    data = NULL;
    siguiente = NULL;
}
 
// Constructor por parámetro
template<typename T>
NodoLista<T>::NodoLista(T data_){
    
    data = data_;
    siguiente = NULL;

}
// Imprimir un Nodo
template<typename T>
void NodoLista<T>::print(){
    
    //cout << "Node-> " << "Dato: " << dato << " Direcion: " << this << " Siguiente: " << next << endl;
    cout << data << "-> ";
}

 
template<typename T>
NodoLista<T>::~NodoLista() {}