#ifndef NODOLISTA_H
#define NODOLISTA_H

#include <iostream>
using namespace std;

 
template <class T>
 
class NodoLista{
    
    public:
        NodoLista();
        NodoLista(T);
        ~NodoLista();

        NodoLista *siguiente;
        T data;
        
        void print();

};
 
#endif // NODE_H