#ifndef NODO_H
#define NODO_H
#include <iostream>
using namespace std;

class Nodo{
    private:
        int valor;
        Nodo* siguiente;
        friend class Pila;
    public:
        Nodo(int v, Nodo* sig = NULL);
        ~Nodo();
};

typedef Nodo *pNodo;
#endif // NODO_H