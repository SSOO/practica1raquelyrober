#include "Pila.h"

Pila::Pila(){
    cima = NULL;
}

Pila::~Pila(){
    pNodo aux;
    while(cima){
        aux = cima;
        cima = cima->siguiente;
        delete aux;
    }    
}

void Pila::apilar(int v){
    pNodo nuevo; 
    nuevo = new Nodo(v,cima);
    cima = nuevo;
}

int Pila::desapilar(){
    pNodo nodo;
    int v;
    if (!cima){
        return 0;
    }
    nodo = cima;
    cima = nodo->siguiente;
    v = nodo->valor;
    delete nodo;
    return v;
}

int Pila :: cimaP(){
    int c;
    c = cima->valor;
    return c; 
}

void Pila :: mostrar() {
    pNodo aux;
    aux = cima; 
    //cout << "PILA :";
    while (aux) {
        cout << "  " << aux->valor;   
        aux = aux->siguiente;
    }
    cout << endl;
}

bool Pila :: esVacia(){
    if (cima == NULL){
        return true;
    } else {
        return false;
    }
}