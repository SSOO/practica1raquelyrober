#ifndef LISTA_H
#define LISTA_H

#include <iostream>
#include <string>
#include <stdlib.h>
 
#include "NodoLista.cpp"
 
using namespace std;
 
template <class T>

class Lista{
    
    public:
        Lista();
        ~Lista();
 
        void insCab(T);
        void insFin(T);
        void eliminarPos(int);
        void print();
        void search(T);
        T verPos(int);

        NodoLista<T> *m_cab;
        int m_num_nodes;
 
    //private:
        
};

 
#endif // LISTA_H