#include "Lista.h"

using namespace std;
 
template<typename T>

Lista<T>::Lista(){      // Constructor por defecto
    
    m_num_nodes = 0;
    m_cab = NULL;
    
}


template<typename T>

void Lista<T>::insCab(T data_){    // Insertar al inicio
    
    NodoLista<T> *new_NodoLista = new NodoLista<T> (data_);
    NodoLista<T> *temp = m_cab;
 
    if (!m_cab) {
        m_cab = new_NodoLista;
    } 
    else {
        new_NodoLista->siguiente = m_cab;
        m_cab = new_NodoLista;
 
        while (temp) {
            temp = temp->siguiente;
        }
    }
    m_num_nodes++;
}

// Insertar al final
template<typename T>
void Lista<T>::insFin(T data_){
    
    NodoLista<T> *new_NodoLista = new NodoLista<T> (data_);
    NodoLista<T> *temp = m_cab;
 
    if (!m_cab) {
        
        m_cab = new_NodoLista;
        
    } 
    
    else {
        while (temp->siguiente != NULL) {
            temp = temp->siguiente;
        }

        temp->siguiente = new_NodoLista;
    }
    m_num_nodes++;
}


template<typename T>
void Lista<T>::eliminarPos(int pos){        // Eliminar por posición del NodoLista
    
    NodoLista<T> *temp = m_cab;
    NodoLista<T> *temp1 = temp->siguiente;
 
    if (pos < 1 || pos > m_num_nodes) {
        cout << "Fuera de rango " << endl;
    }
    
    else if (pos == 1) {
        m_cab = temp->siguiente;
    } 
    
    else {
        for (int i = 2; i <= pos; i++) {
            
            if (i == pos) {
                
                NodoLista<T> *aux_node = temp1;
                temp->siguiente = temp1->siguiente;
                delete aux_node;
                m_num_nodes--;
                
            }
            
            temp = temp->siguiente;
            temp1 = temp1->siguiente;
            
        }
    }
}



template<typename T>
void Lista<T>::print(){         // Imprimir la Lista
    
    NodoLista<T> *temp = m_cab;
    if (!m_cab) {
        cout << "La Lista está vacía " << endl;
    } 
    else {
        while (temp) {
            temp->print();
            if (!temp->siguiente) cout << "NULL";
                temp = temp->siguiente;
        }
  }
  cout << endl << endl;
}


template<typename T>
void Lista<T>::search(T data_){         // Buscar el dato de un NodoLista
    
    NodoLista<T> *temp = m_cab;
    int cont = 1;
    int cont2 = 0;
 
    while (temp) {
        if (temp->data == data_) {
            cout << "El dato se encuentra en la posición: " << cont << endl;
            cont2++;
        }
        temp = temp->siguiente;
        cont++;
    }
 
    if (cont2 == 0) {
        cout << "No existe el dato " << endl;
    }
    cout << endl << endl;
}

template<typename T>
T Lista<T>::verPos(int lug){
    
    NodoLista<T> *temp = m_cab;
    
    for(int i=0; i<lug; ++i) {
        
         temp = temp->siguiente;
        
    }

    
    return temp->data;
}

template<typename T>
Lista<T>::~Lista() {}