#ifndef EVALUACION_H
#define EVALUACION_H
#include "Pila.h"

class Evaluacion{
    
private:
    //char evaluacion [40];
public:
    Evaluacion();
    ~Evaluacion();
    bool es_infija();
    int evaluar_expresionInfija(char evaluacion [40]);
    int evaluar_expresionInfija2(char evaluacion [40]);
    Evaluacion completar_parentesis(char evaluacion [40]);
    int evaluar_expresionPostfija(char evaluacion [40]);
    Evaluacion expresionInfija_a_expresionPostfija(char evaluacion [40]);
};

#endif // EVALUACION_H