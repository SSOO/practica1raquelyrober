#ifndef PILA_H
#define PILA_H
#include "NodoPila.h"

class Pila{
    private:
        pNodo cima;
    public:
        Pila(); //cima(NULL){}
        ~Pila();
        void apilar (int v);
        int desapilar();
        void mostrar(); 
        int cimaP();
        bool esVacia();
        
};

#endif // PILA_H
